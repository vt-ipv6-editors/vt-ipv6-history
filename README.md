# VT IPv6 History

These are notes and materials related to the history of IPv6 at VT.  Originally started after it was noted that 2023-01-30 will mark 25 years of production IPv6.

Have a look at the [wiki](https://code.vt.edu/vt-ipv6-editors/vt-ipv6-history/-/wikis/home) which is where most of the working notes are.
